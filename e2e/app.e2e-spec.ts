import { CombatSystemPage } from './app.po';

describe('combat-system App', () => {
  let page: CombatSystemPage;

  beforeEach(() => {
    page = new CombatSystemPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
