import { Component } from '@angular/core';

import { Creature } from './creature/creature';
import { MaterialsService } from './material/materials.service';
import { WeaponsService } from './weapon/weapons.service';
import { Attack } from './attack/attack';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    player1: Creature;
    player2: Creature;
    selectedAttack: Attack;
    selectedTarget: string;

    constructor(private materialsService: MaterialsService, private weaponsService: WeaponsService) {
        this.player1 = new Creature(materialsService);
        this.player2 = new Creature(materialsService);
        this.player1.weapon = weaponsService.getWeapon('Short Sword', materialsService.getMaterial('Copper'));
        this.selectedAttack = this.player1.weapon.attacks[0];

        this.playerAttacks();
    }

    playerAttacks() {
        console.log(this.selectedAttack, this.player2);
        this.player1.performsAttack(this.selectedAttack).onTarget(this.player2);
    }

    setSelectedAttack(attack) {
        this.selectedAttack = attack;
    }
}
