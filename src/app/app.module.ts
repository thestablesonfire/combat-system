import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CombatInterfaceComponent } from './combat-interface/combat-interface.component';
import { MaterialsService } from './material/materials.service';
import { WeaponsService } from './weapon/weapons.service';

@NgModule({
  declarations: [
    AppComponent,
    CombatInterfaceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [MaterialsService, WeaponsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
