import { AttackType } from './attack-type.enum';

export class Attack {
    constructor(
        public name: string,
        public defaultAttackType: AttackType,
        public currentAttackType: AttackType,
        public contactArea: number,
        public velocity: number
    ) {
        this.currentAttackType = defaultAttackType;
    }
}
