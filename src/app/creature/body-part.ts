import { Material } from '../material/material';

export class BodyPart {
    public subParts: BodyPart[][];
    public isDamaged: boolean;

    constructor(public name: string, public material?: Material) {
        this.isDamaged = false;
    }
}
