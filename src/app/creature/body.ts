import { BodyPart } from './body-part';
import { MaterialsService } from '../material/materials.service';

export class Body {
    head: BodyPart;
    // torso: BodyPart;

    constructor(private materialsService: MaterialsService) {
        this.buildHead();
    }

    buildHead() {
        this.head = new BodyPart('Head');
        const materials = this.materialsService.getAllMaterials();

        const hair = new BodyPart('Hair', materials.Hair);
        const skin = new BodyPart('Skin', materials.Skin);
        const leftEye = new BodyPart('Left Eye', materials.Eye);
        const rightEye = new BodyPart('Right Eye', materials.Eye);
        const fat = new BodyPart('Fat', materials.Fat);
        const cartilage = new BodyPart('Cartilage', materials.Cartilage);
        const muscle = new BodyPart('Muscle', materials.Muscle);
        const nerve = new BodyPart('Nerve', materials.Nerve);
        const skull = new BodyPart('Skull', materials.Bone);
        const brain = new BodyPart('Brain', materials.Brain);

        const surface = [hair, skin, leftEye, rightEye];
        this.head.subParts = [surface, [fat], [muscle], [nerve], [skull], [brain]];
    }
}
