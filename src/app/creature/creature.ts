import { Weapon } from '../weapon/weapon';
import { Attack } from '../attack/attack';
import { AttackType } from '../attack/attack-type.enum';
import { Body } from './body';
import { MaterialsService } from '../material/materials.service';

export class Creature {
    skill = 1;
    size = 61200;
    avgSize = 60000;
    strength = 1250;

    body: Body;
    weapon: Weapon;
    currentAttack: Attack;
    currentAttackMomentum: number;
    currentTarget: Creature;

    constructor(private materialsService: MaterialsService) {
        this.body = new Body(materialsService);
    }

    // M = Skill * Size * Str * Vel / (10^6 * (1 + i_Size/(w_density*w_size)))
    performsAttack(attack: Attack) {
        this.currentAttack = attack;
        this.currentAttackMomentum = this.calculateAttackMomentum();

        return this;
    }

    onTarget(target: Creature) {
        this.currentTarget = target;
        const head = target.getBodyParts().head;

        for (let i = 0; i < head.subParts.length; i++) {
            const thisLayer = head.subParts[i];
            const part = thisLayer[Math.floor(Math.random() * thisLayer.length)];

            if (!this.hitPart(part)) {
                break;
            }
        }
    }

    hitPart(layer) {
        console.log(layer.name);
        return this.currentAttack.currentAttackType === AttackType.Edge ?
             this.performEdgedAttack(layer) : this.performBluntAttack(layer.material);
    }

    performEdgedAttack(layer) {
        console.log('edged attack', this.currentAttackMomentum, this.calculateShearResistance(layer.material));
        // Edged attack
        const resistance = this.calculateShearResistance(layer.material);

        if (this.currentAttackMomentum >= resistance) {
            this.attackSuccess(layer, resistance);

            return true;
        } else {
            return this.performBluntAttack(layer.material);
        }
    }

    performBluntAttack(layer) {
        const targetArmor = layer.material;

        console.log('blunt attack', this.currentAttackMomentum, this.calculateImpactResistance(layer.material));

        if (2 * this.weapon.size * this.weapon.material.impactYield > this.currentAttack.contactArea * targetArmor.material.density) {
            const resistance = this.calculateImpactResistance(targetArmor);

            if (this.currentAttackMomentum >= resistance) {
                this.attackSuccess(layer, resistance);

                return true;
            } else {
            if (this.currentAttack.defaultAttackType === AttackType.Edge) {
                    this.currentAttackMomentum *= this.weapon.material.shearElasticity / 50000;
                } else {
                    this.currentAttackMomentum *= this.weapon.material.impactElasticity / 50000;
                }
                this.currentAttack.currentAttackType = AttackType.Blunt;

                return true;
            }
        } else {
            return false;
        }
    }

    calculateAttackMomentum() {
        return this.skill * this.size * this.strength * this.currentAttack.velocity /
            (1000000 * (1 + this.avgSize / (this.weapon.material.density * this.weapon.size)));
    }

    // For edged Weapons, not using quality for now
    // (aSY/wSY + (A+1)*aSF/wSF) * (10 + 2*a_quality) / (Sha * w_quality)
    calculateShearResistance(targetArmor) {
        // const targetArmor = this.currentTarget.getBodyParts().torso.material;
        const weaponMaterial = this.weapon.material;

        return (targetArmor.shearYield / weaponMaterial.shearYield +
            (this.currentAttack.contactArea + 1) * targetArmor.shearFracture / weaponMaterial.shearFracture) * (10 + 2);
    }

    // 2 * w_size * wIY > A * a_density
    // M >= (2*aIF - aIY) * (2 + 0.4*a_quality) * A,
    calculateImpactResistance(targetArmor) {
        const targetMaterial = targetArmor.material;

        return (2 * targetMaterial.impactFracture - targetMaterial.impactYield) * (2 + 0.4) * this.currentAttack.contactArea;
    }

    getBodyParts() {
        return this.body;
    }

    attackSuccess(layer, resistance) {
        this.currentAttackMomentum -= resistance;
        this.damageLayer(layer);
    }

    damageLayer(layer) {
        layer.isDamaged = true;
    }

    equipArmor() {}
}
