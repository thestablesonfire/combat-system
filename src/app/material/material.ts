export class Material {
    constructor(
        public name,
        public value,
        public density,
        public impactYield,
        public impactFracture,
        public impactElasticity,
        public shearYield,
        public shearFracture,
        public shearElasticity
    ) {}
}
