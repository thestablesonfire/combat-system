import { Injectable } from '@angular/core';

import { Material } from './material';

@Injectable()
export class MaterialsService {
    materials: any;

    constructor() {
        this.materials = {
            Bone: new Material('Bone', 1, 500, 200000, 200000, 100, 115000, 130000, 100),
            Brain: new Material('Brain', 1, 500, 10000, 10000, 50000, 20000, 20000, 50000),
            Cartilage: new Material('Cartilage', 1, 500, 10000, 10000, 25000, 30000, 30000, 25000),
            Copper: new Material('Copper', 2, 8930, 245000, 770000, 175, 70000, 220000, 145),
            Eye: new Material('Eye', 1, 500, 10000, 10000, 50000, 20000, 20000, 50000),
            Fat: new Material('Fat', 1, 900, 10000, 10000, 50000, 10000, 10000, 50000),
            Hair: new Material('Hair', 1, 500, 10000, 10000, 100000, 60000, 120000, 100000),
            Iron: new Material('Iron', 10, 7850, 542500, 1085000, 319, 155000, 310000, 189),
            Leather: new Material('Leather', 1, 500, 10000, 10000, 50000, 25000, 25000, 50000),
            Muscle: new Material('Muscle', 1, 1060, 10000, 10000, 50000, 20000, 20000, 50000),
            Nerve: new Material('Nerve', 1, 500, 10000, 10000, 50000, 20000, 20000, 50000),
            Skin: new Material('Skin', 1, 1000, 10000, 10000, 50000, 20000, 20000, 50000),
            Steel: new Material('Steel', 30, 7850, 1505000, 2520000, 940, 430000, 720000, 215)
        };
    }

    getMaterial(mat: string) {
      return this.materials[mat];
    }

    getAllMaterials() {
        return this.materials;
    }
}
