import { Weapon } from './weapon';
import { Attack } from '../attack/attack';
import { AttackType } from '../attack/attack-type.enum';

export class ShortSword extends Weapon {
    attacks = [
        new Attack('Slash', AttackType.Edge, 20000, 4000, 1250),
        // new Attack('Stab', AttackType.Edge, 50, 2000, 1000),
        // new Attack('Flat Slap', AttackType.Blunt, 20000, 4000, 1250),
        // new Attack('Pommel Strike', AttackType.Blunt, 100, 1000, 1000)
    ];

    constructor(material) {
        super('Short Sword', 300, [], 'Sword', false, material);
    }

    getAttacks() {
        return this.attacks;
    }
}
