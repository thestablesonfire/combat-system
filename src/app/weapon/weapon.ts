import { MaterialsService } from '../material/materials.service';
import { Material } from '../material/material';
import { Attack } from '../attack/attack';

export class Weapon {
    constructor(
        public name: string,
        public size: number,
        public attacks: Attack[],
        public skillUsed: string,
        public twoHanded: boolean,
        public material: Material
    ) {}
}
