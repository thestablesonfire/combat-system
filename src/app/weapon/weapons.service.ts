import { Injectable } from '@angular/core';

import { ShortSword } from './short-sword';

@Injectable()
export class WeaponsService {
  weapons: Object;

  constructor() {
    this.weapons = {
      'Short Sword': ShortSword
    };
  }

  getWeapon(type, mat) {
    return this.weapons[type](mat);
  }
}
